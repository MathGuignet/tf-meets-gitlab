terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  backend "http" {
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "eu-west-3"
}

resource "aws_instance" "statebehalf" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t3.micro"
  tags = {
    Name = "statebehalf"
  }
}

